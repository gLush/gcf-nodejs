import * as admin from "firebase-admin";
import * as functions from 'firebase-functions';
// const iot = require('@google-cloud/iot');
// const client = new iot.v1.DeviceManagerClient();

interface Config {
    config: string;
}

// start cloud function
exports.configUpdate = functions.firestore
    // assumes a document whose ID is the same as the deviceid
    .document('device-configs/{deviceId}')
    .onWrite(async (change: functions.Change<admin.firestore.DocumentSnapshot>, context?: functions.EventContext) => {
        if (context) {
            console.log("---> device id = " + context.params.deviceId);

            const config: Config = <Config>change.after!.data();

            if (config["config"]) {
                console.log("---> data = %s", config["config"]);

                const iot = require('@google-cloud/iot');
                const client = new iot.v1.DeviceManagerClient();
                const cloudRegion = 'us-central1';
                const projectId = 'ithermo';
                const registryId = 'vilia-registry';
                const deviceId = context.params.deviceId;
                const commandMessage = config["config"];

                const binaryData = Buffer.from(commandMessage).toString('base64');

                const formattedName = client.devicePath(
                    projectId,
                    cloudRegion,
                    registryId,
                    deviceId
                );

                const request = {
                    name: formattedName,
                    binaryData: binaryData,
                    //subfolder: <your-subfolder>
                };

                try {
                    await client.sendCommandToDevice(request);

                    console.log('Sent command');
                } catch (err) {
                    console.error(err);
                }

            }
            // const request = generateRequest(context.params.deviceId, change.after.data(), false);
            // return client.modifyCloudToDeviceConfig(request);
        } else {
            throw (Error("no context from trigger"));
        }
    });